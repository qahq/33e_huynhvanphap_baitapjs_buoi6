//Bài 1
function handleKetQua() {
  var sum = 0;
  var soNhoNhatNguyenDuong = 0;
  while (sum < 10000) {
    soNhoNhatNguyenDuong++;
    sum = soNhoNhatNguyenDuong + sum;
  }
  document.getElementById(
    "result-1"
  ).innerHTML = `<h2 class="text-secondary">Số nguyên dương nhỏ nhất : ${soNhoNhatNguyenDuong}</h2>`;
}

//Bài 2
function handleTong() {
  var soX = document.getElementById("txt-num-x").value * 1;
  var soN = document.getElementById("txt-num-n").value * 1;
  var soMu = 1;
  var sum = 0;
  for (var i = 1; i <= soN; i++) {
    soMu = soX * soMu;
    sum = sum + soMu;
    document.getElementById(
      "result-2"
    ).innerHTML = `<h2 class="text-secondary"> Tổng : ${sum}</h2>`;
  }
}

//Bài 3
function handleGiaiThua() {
  var numberValue = document.getElementById("txt-number").value * 1;
  var giaiThua = 1;
  for (var j = 1; j <= numberValue; j++) {
    giaiThua = giaiThua * j;
  }
  document.getElementById(
    "result-3"
  ).innerHTML = `<h2 class="text-secondary"> Giai thừa : ${giaiThua}</h2>`;
}

//Bài 4
function handleThe() {
  var contentHTML = "";
  for (var k = 1; k <= 10; k++) {
    if (k % 2 == 0) {
      var content = `<h2 style="background: red"> Div chẵn</h2>`;
      contentHTML += content;
    } else {
      var content = `<h2 style="background: blue"> Div lẻ</h2>`;
      contentHTML += content;
    }
  }
  document.getElementById("result-4").innerHTML = contentHTML;
}

